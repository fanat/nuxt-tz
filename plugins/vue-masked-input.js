import Vue from "vue";
import MaskedInput from "vue-masked-input";
if (process.client) {
  Vue.component("MaskedInput", MaskedInput);
}
