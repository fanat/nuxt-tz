module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: "project_tz",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Nuxt.js project" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3B8070" },
  /*
   ** Build configuration
   */
  plugins: [
    "~/plugins/vue-the-mask",
    { src: "~/plugins/vue-masked-input", ssr: false }
  ],
  modules: ["@nuxtjs/style-resources"],
  css: ["@/assets/scss/main.scss"],
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
    }
  },
  styleResources: {
    scss: ["@/assets/scss/_variables.scss"]
  }
};
